import { Component, Input, Output, EventEmitter } from '@angular/core';
import {NgModel} from "@angular/common";

@Component({
  moduleId: module.id,
  selector: 'app-drop-down-list',
  templateUrl: 'drop-down-list.component.html',
  directives: [NgModel],
  styleUrls: ['drop-down-list.component.css']
})
export class DropDownListComponent{

  @Input() list: Array<number>;
  @Output() mouseEnter = new EventEmitter();
  @Output() mouseLeave = new EventEmitter();
  url: string;
  urlText: string;
  username: string = "Guanyi";

  urlList: Link[];


  constructor() {
    let item1: Link = new Link(1, "https://www.google.com", "Google");
    let item2: Link = new Link(2, "https://www.microsoft.com", "Microsoft");
    let item3: Link = new Link(3, "https://www.apple.com", "Apple");
    this.urlList = new Array<Link>();
    this.urlList.push(item1, item2, item3);
  }

  trigger(value: number) {
    let selectedItem: Link = this.urlList.find(item=>item.id == value);
    this.urlText = selectedItem.urlText;
    this.url = selectedItem.url;
  }

  hover() {
    this.mouseEnter.emit(this.urlList.length);
  }

  leave() {
    this.mouseLeave.emit(this.username);
  }
}

class Link {
  constructor(public id: number, public url: string, public urlText: string) {}
}
