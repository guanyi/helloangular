/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { DropDownListComponent } from './drop-down-list.component';

describe('Component: DropDownList', () => {
  it('should create an instance', () => {
    let component = new DropDownListComponent();
    expect(component).toBeTruthy();
  });
});
