import { Component } from '@angular/core';
import {DropDownListComponent} from './drop-down-list';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  directives: [DropDownListComponent],
  template: `
              <h1> {{title}} </h1>
<!--run npm start-->
<h5>binding</h5>
<h5>form</h5>
<h5>routing</h5>
<h5>pipe</h5>
<h5>http service</h5>
<h5>gulp</h5>
<h5>test karma protractor</h5>
<h5>table, sort, filter</h5>
<h5>integrate with bootstrap</h5>

<app-drop-down-list [list]="indexList" (mouseEnter)="onFirstOutputCalled($event)" (mouseLeave)="onSecondOutputCalled($event)"></app-drop-down-list>
             `,
  styleUrls: ['app.component.css']
})
export class AppComponent {
  title: string = 'app works!';
  indexList: Array<number> = [1, 2, 3, 4, 5];

  onFirstOutputCalled($event) {
    this.title = "There are " + $event + " number of items";
  }

  onSecondOutputCalled($event) {
    this.title = $event;
  }
}
